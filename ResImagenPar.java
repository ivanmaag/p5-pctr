
/**
 * Práctica 5 - PCTR 2020
 * 
 * @author Iván Magariño Aguilar
 * @version 1.0
 */

import java.util.*;
import java.util.concurrent.*;

public class ResImagenPar implements Runnable {

	static int n=20000;
	static int imagen[][]=new int[n][n];
    private int numero;
    
	public ResImagenPar(int numero) {
		this.numero=numero;
    }
    
	public static void iniciarImagen() {
		Random aleatorio=new Random();
		for(int i=0;i<n;i++){
			for(int j=0;j<n;j++){
				imagen[i][j]=aleatorio.nextInt(20);
			}
		}
    }
    
	public static void imprimir() {
		for(int i=0;i<n;i++){
			for(int j=0;j<n;j++){
				System.out.print(" "+imagen[i][j]+" ");
			}
			System.out.println();
		}
    }
    
	public void run() {
		int i=numero;
		for(int j=0;j<n;j++){
			int ianterior=i-1;
			int janterior=j-1;
			if(janterior<0)janterior=n-1;
			if(ianterior<0)ianterior=n-1;
			synchronized(imagen){
				imagen[i][j]=(4*imagen[i][j]-imagen[(i+1)%n][j]-imagen[i][(j+1)%n]-imagen[ianterior][j]-imagen[i][janterior])/8;
			}
		}
    }
    
    /**
     * Método main.
     * @param args
     * @throws Exception
     */
	public static void main(String[] args) throws Exception {
		iniciarImagen();
		System.out.println("Imagen sin tratar");
		//imprimir();
		Date d = new Date();
		long inicCronom = System.currentTimeMillis(); //se prepara el cronometro
		d.setTime(inicCronom); //se activa el cronometro
		ExecutorService pool= Executors.newFixedThreadPool(n);
		for(int i=0;i<n;i++){
			pool.execute(new ResImagenPar(i));
		}
		pool.shutdown();
		pool.awaitTermination(1,TimeUnit.DAYS);
		long finCronom = System.currentTimeMillis(); //se para el cronometro
		d.setTime(finCronom);
		System.out.println("imagen tratada");
		//imprimir();
		System.out.println("Tiempo: "+ (finCronom - inicCronom) + " milisegundos");
	}
}
