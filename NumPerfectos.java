
/**
 * Práctica 5 - PCTR 2020
 * 
 * @author Iván Magariño Aguilar
 * @version 1.0
 */

import java.util.concurrent.*;
import java.util.*;

public class NumPerfectos implements Callable {
   
    private final long linf;
    private final long lsup;
    private Long total = new Long(0);
    private static int suma=0;
   	
    public NumPerfectos(long linf, long lsup) {
        this.linf = linf;
        this.lsup = lsup;
    }
  
    public static boolean esPerfecto(long n) {
        if (n<=1) return(false);
        suma=0;
        for (int i=1; i<n; i++) { //i son los divisores. Se divide desde 1 hasta n-1
            if (n%i == 0)
                suma+=i;  //si es divisor se suma
        }
        if (suma==n) return(true);
        else return(false);
    }
		
    public Long call() {   
        for (long i=linf; i<=lsup;i++)
            if (esPerfecto(i))
                total++;
        return(total);
    }

    /**
     * Método main.
     * @param args Recibe un argumento por línea de comandos.
     * @throws Exception Se lanzan excepciones.
     */
    public static void main(String[] args) throws Exception {
        long nPuntos        = Integer.parseInt(args[0]);
        int  nTareas        = Runtime.getRuntime().availableProcessors();
        long tVentana       = nPuntos/nTareas;
        long perfectosTotal = 0;
        long linf           = 0;
        long lsup           = tVentana;
        
        ArrayList<Future<Long>> contParciales = new ArrayList<Future<Long>>();
        long inicTiempo = System.nanoTime();  
        ThreadPoolExecutor ept = new ThreadPoolExecutor(
          nTareas,
          nTareas,
          0L,
          TimeUnit.MILLISECONDS,
          new LinkedBlockingQueue<Runnable>());
        
          for(int i=0; i<nTareas; i++) {
            contParciales.add(ept.submit(new NumPerfectos(linf, lsup)));
            linf=lsup+1;
            lsup+=tVentana;
        }  
        for(Future<Long> iterador:contParciales)
          try{
                perfectosTotal +=  iterador.get(); 
          }catch (CancellationException e){}
           catch (ExecutionException e){}
           catch (InterruptedException e){}     
        long tiempoTotal = (System.nanoTime()-inicTiempo)/(long)1.0e9;   
        ept.shutdown();
        System.out.println("Perfectos hallados: "+perfectosTotal);
        System.out.println("Calculo finalizado en "+tiempoTotal+" segundos");
    }   
}
