
/**
 * Práctica 5 - PCTR 2020
 * 
 * @author Iván Magariño Aguilar
 * @version 1.0
 */

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ProdMatricesParalelo implements Runnable {

    public  static int n,m,suma;
    private  int hilo;
    public	static int A[][];
    public 	static int b[];
    public  static int sol[];

    /** Constructor de la clase runnable
      * @param tipohilo contiene la fila que multiplicara ese hilo 
      */
    public ProdMatricesParalelo(int tipohilo) {
        hilo=tipohilo;
        
    }

    /** Funcion run
      * realiza las operacio de multiplicar las filas de los hilos por la matriz columna
      */
    public void run() {
        suma=0;
        for(int j=0;j<m;j++){
            suma=suma+A[hilo][j]*b[j];
        }
        sol[hilo]=suma;
    }

    /** Funcion Subramanian 
      * inicializa las matrices con dimensiones y crea un vector de hebras dependiendo de el numero de filas de la matriz
      * Muestra el tiempo que ha tardado en resolverlo en pantalla
      */
    public static int Subramanian(int Nn,double cb) {
        double Nt;
        int res;
        Nt=(double)Nn/(1-cb);
        System.out.println("se ejecutaran "+Nt+" Hebras");
        res=(int)Math.round(Nt);
        
        return res;
    }
    
    /**
     * Método main.
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {

        Scanner sc=new Scanner(System.in);
        int opcion;
        int Nt;

        System.out.println("Introduzca las dimensiones de la matriz");
        n=sc.nextInt();
        m=sc.nextInt();
        A=new int[n][m];
        b=new int[m];
        sol=new int[m];
        int nProc = Runtime.getRuntime().availableProcessors();
        Nt=Subramanian(nProc,0.5);

        System.out.print("1.Introducir los datos manualmente\n2.Introducir los datos de manera aleatoria\n");
        opcion=sc.nextInt();
        switch(opcion){
            case 1:
                for(int i=0;i<n;i++){
                    for(int j=0;j<m;j++){
                        System.out.println("Introduzca el valor de la posicion de la matriz ["+i+"]["+j+"]");
                        A[i][j]=sc.nextInt();
                    }
                    
                }
                for(int k=0;k<m;k++){
                    System.out.println("Introduzca el  valor de la posicion "+k+" de la matriz columna");
                    b[k]=sc.nextInt();
                }
                break;
            case 2:
                Random aleatorio=new Random();
                for(int i=0;i<n;i++){
                    for(int j=0;j<m;j++){
                        A[i][j]=aleatorio.nextInt();
                    }
                    
                }
                for(int k=0;k<m;k++){
                    b[k]=aleatorio.nextInt();
                }
                break;
        }  
            
        Date d = new Date();
        long inicCronom = System.currentTimeMillis(); //se prepara el cronometro
        d.setTime(inicCronom); //se activa el cronometro
        ExecutorService exe= Executors.newFixedThreadPool(Nt);
        for(int i=0;i<n;i++){
            exe.execute(new ProdMatricesParalelo(i));
        }
        exe.shutdown();
        while(!exe.isTerminated()){}
        long finCronom = System.currentTimeMillis(); //se para el cronometro
        d.setTime(finCronom);
        System.out.println("Tiempo: "+ (finCronom - inicCronom) + " milisegundos");
        /**System.out.println("La matriz solucion es ");
        for(int k=0;k<n;k++){
            System.out.println(sol[k]);
        }*/

    }
}
    